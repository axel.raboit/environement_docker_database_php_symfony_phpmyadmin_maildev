/!\This depo can't run the gitlab ci (issue about the cache)/!\

###Procedure pour faire la base d'un projet avec Docker

- PHP -> wwww -> Build (vhosts)
- Database -> Image
- phpMyAdmin -> Image
- MailDev -> Image


```bash
docker-compose up -d (cela va builder et démarrer les containers)
```

#Installation de Symfony

-> Rentrer dans le container www
```bash
docker exec -it www_docker_symfony bash
```

-> Installation de Symfony
```bash
symfony new project --full
```

-> Donner les droits
```bash
sudo chown -R $USER ./
```

#Creation de la base de donnée

-> dans le fichier env
DATABASE_URL=mysql://root:@db_docker_symfony:3306/docker_project?serverVersion=5.7

-> dans le dossier project du container www
```bash
php bin/console doctrine:database:create
```

#Acces à la base de donnée
-> phpMyAdmin
http://localhost:8080/
user: root / password: [empty]

-> container db
```bash
docker exec -it db_docker_symfony bash
mysql -u root;
```

#Acces au projet symfony dans le navigateur
-> http://localhost:8741/



